/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "spi.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "board.h"
#include "string.h"
#include "radio.h"
#include "sx1276_78-LoRa.h"
#include "sx1276_78-LoRaMisc.h"
#include "platform.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
uint8_t debug_send[14]={0x68, 0x28, 0x62, 0x06, 0x15, 0x20, 0x00, 0x68, 0x01, 0x02, 0x52, 0xC3, 0xAD, 0x16};
uint8_t debug_recv[100] = {0};
uint8_t flag_send=0;
uint8_t data_recv[14];
uint8_t flag_recv=0;

uint8_t debug_count=0;

uint8_t count_node_id[4] = {1, 2, 3, 4};

extern tLoRaSettings LoRaSettings;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
#define BUFFER_SIZE                                 100 // Define the payload size here
#define newline 				"\r\n"

static uint16_t BufferSize = BUFFER_SIZE;			// RF buffer size
static uint8_t Buffer[BUFFER_SIZE];					// RF buffer


tRadioDriver *Radio = NULL;

char *authen = "DC*j6kg3d*10*5*16*0000000000000000";
uint8_t authen_len = 34;
char *data_send = "DC*j6kg3d*30*5*42*";
uint8_t data_len = 18;
uint8_t body_data[42];


uint8_t count = 0;




void Lora_Tx()
{
	uint8_t i;
	//HAL_Delay(2000);
	// Send packet: PING
	//char *buf_debug = "PING567890123456789012345678901234567890134567890PING567890123456789012345678901234567890134567890";
	memset(Buffer, 0, BufferSize);
	for( i = 0; i < authen_len; i++ )
	{
			Buffer[i] = authen[i];
	}
	Radio->SetTxPacket( Buffer, authen_len );
	while(!flag_send)
	{
		Radio->Process();
	}
	flag_send=0;
}

void Lora_Tx_data()
{
	uint8_t i;
	double PH_data = 1;
	double temp_data = 28;
	double oxy_data = 10;
	double salinity_data = 8;
	uint8_t data_convert[8];
	
	//HAL_Delay(2000);
	// Send packet: PING
	//char *buf_debug = "PING567890123456789012345678901234567890134567890PING567890123456789012345678901234567890134567890";
	count++;
	if(count > 10)
		count = 0;
	
	body_data[0] = 0;
	body_data[1] = 4;
	
	body_data[2] = 0;
	body_data[3] = 1;	
	PH_data += count;
	uint8_t *data_byte = (uint8_t *)&PH_data;
	*(double *)data_convert = PH_data;
	for(uint8_t j=0; j<8; j++)
	{
		body_data[j+4] = data_byte[7-j];
	}
	
	body_data[12] = 0;
	body_data[13] = 2;	
	temp_data += count;
	*(double *)data_convert = temp_data;
	for(uint8_t j=0; j<8; j++)
	{
		body_data[j+14] = data_convert[7-j];
	}

	body_data[22] = 0;
	body_data[23] = 3;	
	oxy_data -= count;
	*(double *)data_convert = oxy_data;
	for(uint8_t j=0; j<8; j++)
	{
		body_data[j+24] = data_convert[7-j];
	}

	body_data[32] = 0;
	body_data[33] = 4;	
	salinity_data += count;
	*(double *)data_convert = salinity_data;
	for(uint8_t j=0; j<8; j++)
	{
		body_data[j+34] = data_convert[7-j];
	}	

	for( i = 0; i < data_len; i++ )
	{
			Buffer[i] = data_send[i];
	}
	
	for( i = 0; i < 42; i++ )
	{
			Buffer[i+data_len] = body_data[i];
	}
	Radio->SetTxPacket( Buffer, data_len + 42 );
	while(!flag_send)
	{
		Radio->Process();
	}
	flag_send=0;
}


void Lora_Rx(uint8_t *data)
{
	switch(Radio->Process())
	{
		case RF_RX_DONE:
			Radio->GetRxPacket(data, ( uint16_t* )&BufferSize );
			flag_recv=1;
			break;
	}
}

void data_proccess(uint8_t *p_data)
{
	// check NODE_ID
	

}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI1_Init();
  /* USER CODE BEGIN 2 */
	BoardInit();
	Radio = RadioDriverInit();
	LoRaSettings.SpreadingFactor = 7;
	Radio->Init();
	Radio->StartRx();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
			HAL_Delay(1000);
			Lora_Tx_data();
			//Radio->StartRx();
			//Lora_Rx(debug_recv);
//		if(debug_recv[0] == 'D'){
//			LoRaSettings.SpreadingFactor = 7;
//			Radio->Init();
//			//HAL_Delay(1000);

//			Lora_Tx();
//			LoRaSettings.SpreadingFactor = 8;
//			Radio->Init();
//			Radio->StartRx();
//			debug_recv[0] = 0;
//		}
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
