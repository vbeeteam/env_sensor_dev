#ifndef __M880A_HAL_H__
#define __M880A_HAL_H__

#include "platform.h"

#include <stdint.h>
#include <stdbool.h>

#include "stm32f0xx_hal.h"


/*!
 *  SPI DEFINES
*/
#define SPI_NAME 																		&hspi1


/*!
 * SX127x definitions
 */

/*!
 * SX1272 RESET I/O definitions
 */
#define RESET_IOPORT                                GPIOA
#define RESET_PIN                                   GPIO_PIN_1

/*!
 * SX1272 SPI NSS I/O definitions
 */
#define NSS_IOPORT                                  GPIOA
#define NSS_PIN                                     GPIO_PIN_4

/*!
 * SX1272 DIO pins  I/O definitions
 */
#define DIO0_IOPORT                                 GPIOB
#define DIO0_PIN                                    GPIO_PIN_1

#define DIO1_IOPORT                                 GPIOA
#define DIO1_PIN                                    GPIO_PIN_10

#define DIO2_IOPORT                                 GPIOA
#define DIO2_PIN                                    GPIO_PIN_10

#define DIO3_IOPORT                                 GPIOA
#define DIO3_PIN                                    GPIO_PIN_10

#define DIO4_IOPORT                                 GPIOA
#define DIO4_PIN                                    GPIO_PIN_10

#define DIO5_IOPORT                                 GPIOA
#define DIO5_PIN                                    GPIO_PIN_10

//FEM_CTX_PIN
#define RXTX_IOPORT                                 GPIOA
#define RXTX_PIN                                    GPIO_PIN_10



/*!
 * Initializes board peripherals
 */
void BoardInit( void );

#endif //__M880A_HAL_H__

